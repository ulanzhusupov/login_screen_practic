import 'package:flutter/material.dart';
import 'package:flutter_purple_login_page/presentation/themes/app_fonts.dart';
import 'package:flutter_purple_login_page/presentation/widgets/custom_text_field.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:google_fonts/google_fonts.dart';

class FisrtScreen extends StatelessWidget {
  const FisrtScreen({super.key});

  @override
  Widget build(BuildContext context) {
    TextEditingController phoneController = TextEditingController();

    return Container(
      decoration: const BoxDecoration(
          gradient: LinearGradient(colors: [
        Color(0xFFFF00F5),
        Color(0xFF00CCFF),
      ], begin: Alignment.topCenter, end: Alignment.bottomCenter)),
      child: Scaffold(
        backgroundColor: Colors.transparent,
        body: SingleChildScrollView(
          child: SafeArea(
            child: Stack(
              children: [
                Positioned(
                  top: 100.h,
                  left: 0,
                  child: Image.asset(
                    "assets/ellipseMin.png",
                    width: 215.w,
                  ),
                ),
                Positioned(
                  right: 0,
                  bottom: 50,
                  child: Image.asset(
                    "assets/ellipseMax.png",
                    width: 287.w,
                  ),
                ),
                Positioned.fill(
                  child: Align(
                    alignment: Alignment.centerRight,
                    child: Container(
                      width: double.infinity,
                      height: 565.h,
                      decoration: BoxDecoration(
                        color: Colors.white.withOpacity(0.20),
                        shape: BoxShape.rectangle,
                        borderRadius: const BorderRadius.all(
                          Radius.circular(40),
                        ),
                      ),
                      child: SizedBox(
                        child: Column(
                          children: [
                            SizedBox(height: 23.h),
                            Text("Welcome", style: AppFonts.s25W600),
                            SizedBox(height: 139.h),
                            Padding(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 49),
                              child: CustomTextField(
                                controller: phoneController,
                                hintText: "Phone",
                              ),
                            ),
                            SizedBox(height: 119.h),
                            ElevatedButton(
                              onPressed: () {},
                              style: ElevatedButton.styleFrom(
                                backgroundColor: Colors.white.withOpacity(0.3),
                                shadowColor: Colors.transparent,
                              ),
                              child: Padding(
                                padding: EdgeInsets.symmetric(
                                  horizontal: 30.w,
                                  vertical: 17.h,
                                ),
                                child: Text(
                                  "Sign In",
                                  style: AppFonts.s15W600,
                                ),
                              ),
                            ),
                            const Spacer(),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text(
                                  "Are you a new user?",
                                  style: AppFonts.s15W600,
                                ),
                                TextButton(
                                  onPressed: () {},
                                  child: Text("Sign Up",
                                      style: AppFonts.s15W600.copyWith(
                                          color: const Color(0xff02FFF0))),
                                )
                              ],
                            ),
                            SizedBox(height: 19.h),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
