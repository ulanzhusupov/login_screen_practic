import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

abstract class AppFonts {
  static TextStyle s25W600 = const TextStyle(
    fontWeight: FontWeight.w600,
    fontSize: 25,
    color: Colors.white,
  );
  static TextStyle s15W600 = const TextStyle(
    fontSize: 15,
    fontWeight: FontWeight.w600,
    color: Colors.white,
  );
}
